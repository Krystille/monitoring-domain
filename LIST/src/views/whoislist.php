<div id="whois-table" class="row">
	<div class="col m12">
		<table class="highlight">
			<thead>
				<tr>
					<th>name</th>
					<th>registrar</th>
					<th>domain status</th>
					<th>name servers</th>
					<th>date</th>

				</tr>
			</thead>
			<tbody>
				<tr>
					<td>domain.com</td>
					<td>
						<span>registrar</span> <br>
						<span>domain id:  2211496739_DOMAIN_COM-VRSN</span><br>
						<span>whois server:  2211496739_DOMAIN_COM-VRSN</span><br>
						<span>url</span><br>
						<span>iana id</span><br>
						<span>abuse email</span><br>
						<span>abuse phone</span>
					</td>
					<td>
						<span>renew: 2018-06-21T17:45:28Z</span><br>
						<span>transfer: 2018-06-21T17:45:28Z</span><br>
						<span>update:2018-06-21T17:45:28Z</span><br>
						<span>delete: 2018-06-21T17:45:28Z</span>
					</td>
					<td>
						<span>NS1.NAMEBRIGHTDNS.COM</span><br>
						<span>NS1.NAMEBRIGHTDNS.COM</span><br>
						<span>NS1.NAMEBRIGHTDNS.COM</span><br>
						<span>NS1.NAMEBRIGHTDNS.COM</span>
					</td>
					<td>
						<span>created:2018-06-26T09:28:42Z</span><br>
						<span>updated:2018-06-26T09:28:42Z</span><br>
						<span>expiry:2018-06-26T09:28:42Z</span><br>
					</td>
				</tr>
			</tbody>
			
		</table>
	</div>
</div>