<?php
include('../PHP/session.php');
include('../header.php');
?>
<!-- Page Content -->
<section>
  <div class="container contentDiv">
    <!-- <div class="row">
      <div class="col-lg-12">
        <form class="form-inline justify-content-center">
          <label>Channel:</label>&nbsp;
           <select class="form-control" id="channelPricingSelect">
            <option value="" selected disabled>-Please select-</option>
            <option value="All">All</option>
            <option value="Afternic">Afternic</option>
            <option value="Sedo">Sedo</option>
            <option value="GoDaddy">GoDaddy</option>
          </select>&nbsp;&nbsp;
          <button type="button" class="btn btn-success" onclick="viewDomainPricing();">GENERATE</button>
        </form>
      </div>
    </div><br> -->

    <div class="row">
      <div class="col-lg-12">

        <?php
          include('../PHP/connection.php');
          $ReadSql = "SELECT main.domain_name, main.domain_price, a.price AS afternicPrice, s.price as sedoPrice, a.status AS afternicStatus, s.status AS sedoStatus FROM tbl_domainpricing main INNER JOIN afternic a ON main.id = a.domain_id INNER JOIN sedo s ON main.id = s.domain_id";
          $res = mysqli_query($conn, $ReadSql); 

          if ($res->num_rows > 0) {
            echo "<script>plsWaitDiv('body', 'show');</script>";
        ?>
        <table class="table table-bordered nowrap display" id="tbldomainCompare">
          <thead>
            <tr>
              <th>Domain Name</th>
              <th>System Price(USD)</th>
              <th>Afternic Price(USD)</th>
              <th>Afternic Status</th>
              <th>Sedo Price(USD)</th>
              <th>Sedo Status</th>
              <!-- <th>GoDaddy Price(USD)</th> -->
              <th>Delete</th>
            </tr>
          </thead>
          <tbody id="tbodyidprice">
            <?php 
              while($row = $res->fetch_assoc()) {
                $num = floatval(str_replace('$', '', $row["domain_price"]));
                echo "<tr><td>" . $row["domain_name"]. "</td><td>" . number_format($num, 2). "</td><td>" . $row["afternicPrice"]. "</td><td>" . $row["afternicStatus"]. "</td><td>" . $row["sedoPrice"]. "</td><td>" . $row["sedoStatus"]. "</td><td><button class='btn btn-danger btn-sm' onclick='deleteDomain(".$row['id'].")'><span class='fa fa-trash'></span></button></td></tr>";
              }
              echo "<script>plsWaitDiv('body', 'hide');</script>";
            ?>
          </tbody>
        </table>
        <?php 
          }
          else
            echo "<br><center><h4>No List Yet</h4></center>"; 
        ?>
      </div>
    </div>
  </div>
</section>