 <?php
  include realpath(dirname(__FILE__)).'/src/class/Directory.php';
  
  $path = array(
    'materializecss' => MATERIALCSS,
    'materializejs' => MATERIALJS,
    'jquery' => JQUERY,
    'mainjs' => MAINJS,
    'maincss' =>MAINCSS
  );

  include realpath(dirname(__FILE__)).'/src/views/head.php';
  include realpath(dirname(__FILE__)).'/src/views/body.php';
  include realpath(dirname(__FILE__)).'/src/views/foot.php';
?>
   