<?php
	include 'Domain.php';
	$domain = new Domain();
	$data = '';

	if( ! isset( $_POST[ 'request' ] ) ) return;

	$request =  $domain->sanitize( $_POST[ 'request' ]  ,false);

	switch ( $request ) {
		case 'get':
			$data = $domain->get( 
					 json_decode( $_POST['filters'] ,true )// cannot sanitize, it convert the boolean to string
			);
			break;
		
		default:
			$data = '';
			break;
	}

	echo $data;
?>