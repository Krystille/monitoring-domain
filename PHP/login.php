<?php
include 'connection.php';
session_start(); // Starting Session
$error=''; // Variable To Store Error Message
if (isset($_POST['submit-login'])) {
	if (empty($_POST['username']) || empty($_POST['password'])) {
		echo "<script type=\"text/javascript\">
		alert(\"Username or Password is invalid.\");
		window.location = \"../pages/loginPage.php\";
		</script>";

		exit(0);
	}
	else
	{
		// Define $username and $password
		$username=$_POST['username'];
		$password=$_POST['password'];
		
		// To protect MySQL injection for Security purpose
		$username = stripslashes($username);
		$password = stripslashes($password);
		$username = mysqli_real_escape_string($conn, $username);
		$password = md5(mysqli_real_escape_string($conn, $password));
		
		// SQL query to fetch information of registerd users and finds user match.
		$query = mysqli_query($conn, "select * from tbl_users where password='$password' AND username='$username'");
		$rows = mysqli_num_rows($query);
		if ($rows == 1) {
			$_SESSION['login_user']=$username; // Initializing Session
			while ($data = mysqli_fetch_assoc($query)) {
				$_SESSION['login_role']=$data['role'];
				$_SESSION['login_id']=$data['id'];
			}
			header("location: ../pages/home.php"); // Redirecting To Other Page
		} else {
			echo "<script type=\"text/javascript\">
			alert(\"Username or Password is invalid\");
			window.location = \"../pages/loginPage.php\";
			</script>";

			exit(0);
		}
		mysqli_close($conn); // Closing Connection
	}
}
?>