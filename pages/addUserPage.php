<?php
include('../PHP/session.php');
include('../header.php');
?>
<!-- Page Content -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
            <div class="login-form">    
                <form method="post" action="../PHP/addUser.php">
                    <h4 class="modal-title">Add User Account</h4>
                    <div class="form-group">
                        <input type="text" class="form-control" name="usernameNew" placeholder="Username" required="required">
                    </div>
                    <div class="form-group">
                        <input type="Email" class="form-control" name="emailNew" placeholder="Email" required="required">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="passwordNew" id="passwordNew" placeholder="Password" onkeyup="check();" required="required">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="confirmPasswordNew" id="confirmPasswordNew" placeholder="Confirm Password" onkeyup="check();" required="required">
                        <div id="message"></div>
                    </div> 
                    <input type="submit" name="submit-new" class="btn btn-success btn-block btn-lg" value="Add">              
                </form>         
            </div>
      </div>
    </div>
  </div>
</section>

<?php
//include('../footer.php');
?>