<?php
include('../PHP/session.php');
include('../header.php');
?>
<!-- Page Content -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12" style="margin-top: 100px;">
        <div class="row">
          <div class="col-sm-4"></div>
            <div class="col-sm-4">
               <div class="login-panel panel panel-success">
                <div class="panel-heading">
                  <h3 class="panel-title text-center">Upload Domain Pricing</h3><br> 
                </div>
                <div class="panel-body">
                  <form method="post" id="uploadFormPricing" onsubmit="return uploadFilePricing();">
                   <fieldset>
                    <input type="hidden" name="channel" value="Global" >
                    <div class="custom-file inputUpload">
                      <input type="file" class="custom-file-input" name="file" id="customFile" accept=".csv" required="required">
                      <label class="custom-file-label" for="customFile">Choose file</label>
                    </div><br>
                    <label class="noteFile">Please make sure the file is in .csv format</label><br> 
                    <input class="btn btn-success" type="submit" name="submit_file" value="Upload"/>
                   </fieldset>
                  </form>
                </div>
              </div>
            </div>
            <div class="col-sm-4"></div>
        </div> 
    </div>
  </div>
</div>
</section>

<?php
//include('../footer.php');
?>