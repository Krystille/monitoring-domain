<?php
	$filters = json_decode( $_POST['filters'] , true );

	$field = $filters['field'];
	$keyword = $filters['keyword'];
	$pagination = $filters[ 'pagination'];
?>
<div id="filter-trigger" class="z-depth-3">	
	<div data-target="filter-menu" class=" arrow-content valign-wrapper sidenav-trigger">
		<a href="" class="btn waves-effect waves-light teal lighten-1"><i class="material-icons right">filter_list</i>filters</a>
	</div>	
</div>

<div id="filter-menu" class="sidenav blue-grey lighten-4">
	<table class="striped hoverable blue-grey lighten-4">
		<thead>
			<tr>
				<th>Host</th>
				<th>Price</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
			<?php
				$host = array( 'Afternic' ,'sedo' , 'godaddy' );
				foreach ($field as $key => $value) {
					if( array_key_exists( 'details' , $value ) ){ 
						$price = $value['details']['price'] ? 'checked="checked"' : '';
						$status = $value['details']['status'] ? 'checked="checked"' : '';
				?>
					<tr>
						<td><?php echo $value['title']; ?></td>
						<td>
						      <label>
						        <input 
						        	id="<?php echo $value['name'].'.details.price'?>" 
						        	class="filter_fields_checkbox" 
						        	type="checkbox" 
						        	<?php echo $price; ?>
						        />
						        <span></span>
						      </label>
						</td>
						<td>
						      <label>
						        <input  
						        	id="<?php echo $value['name'].'.details.status'?>" 
						        	class="filter_fields_checkbox" 
						        	type="checkbox" 
						        	<?php echo $status; ?>
						        />
						        <span></span>
						      </label>
						</td>
					</tr>
				<?php
					}
				}
			?>
			<tr>
				<td colspan="3"><div class="input-field">
					<input 
						id="filter_keywords" 
						class="filter_fields active" 
						type="text" 
						value="<?php echo $keyword; ?>"
					>
					<label for="filter_keywords">Domain</label>
				</div></td>	
				
			</tr>
			<tr>
				
				<td colspan="3">

					<div class="page-range">
						<span>page: <?php echo $pagination['itemLength'];?></span>
					    <p class="range-field">
					      <input type="range" id="itemLength" min="10" max="100" value="10"/>
					    </p>
					</div>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<input 
						id="apply-filter" 
						type="button" 
						name="filter" 
						value="Apply filter" 
						class="btn btn-small"
					>
				</td>
			</tr>
		</tbody>
	</table>	

</div>