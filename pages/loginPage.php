<?php
include('../PHP/login.php');
if(isset($_SESSION['login_user']))
    echo "<script type='text/javascript'> document.location = 'home.php'; </script>";
include('../header.php');
?>
<!-- Page Content -->
<section>
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
            <div class="login-form">    
                <form method="post" action="../PHP/login.php">
                    <div class="avatar"><i class="material-icons">&#xE7FF;</i></div>
                    <h4 class="modal-title">Login to Your Account</h4>
                    <div class="form-group">
                        <input type="text" class="form-control" name="username" placeholder="Username" required="required">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password" required="required">
                    </div>
                    <div class="form-group small clearfix">
                        <label class="checkbox-inline"><input type="checkbox"> Remember me</label>
                        <a href="" data-toggle="modal" data-target="#forgotPassModal" class="forgot-link">Forgot Password?</a>
                    </div> 
                    <input type="submit" name="submit-login" class="btn btn-success btn-block btn-lg" value="Login">              
                </form>         
                <!-- <div class="text-center small">Don't have an account? <a href="#">Sign up</a></div> -->
            </div>
      </div>
    </div>

    <div class="modal fade" id="forgotPassModal" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog modal-dialog-centered">
          <div class="modal-content">
          
            <!-- Modal Header -->
            <div class="modal-header">
              <h4 class="modal-title text-center"><center>Reset Password</center></h4>
              <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <form method="post" action="../PHP/forgotPassword.php">
            <div class="modal-body">
              <div>
                <label>Email Address: </label>
                <input type="email" class="form-control input-sm" name="emailFP" placeholder="Email" required="required">
              </div>
            </div>
            
            <!-- Modal footer -->
            <div class="modal-footer">
              <button type="submit" class="btn btn-success mr-auto">SEND PASSWORD RESET</button>
            </div>
            </form>
          </div>
        </div>
      </div>

  </div>
</section>

<?php
//include('../footer.php');
?>