<?php
	$fieldStructure = json_decode( $_POST['fieldname'] ,true );
	$fieldvalue =  json_decode( $_POST['fieldvalue'] , true );
	$sortby = $_POST['sortby'];
	$sortOption = array( 'asc' => 'arrow_drop_down' , 'desc' => 'arrow_drop_up' );


?>
<div class="row">

	<div class="col m12">
		<table class="highlight">
			<thead>
				<tr>
					<?php
						foreach ( $fieldStructure as $key => $value) {
							if( $value['show'] ){
								if( $sortby['field'] == $value['name'] ){
									$sortIcon = $sortOption[ $sortby['value'] ];
									$order = $sortby['value'];
								}else {
									$sortIcon = $sortOption['asc'];
									$order = 'asc';
								}
												 	
							?>
								<?php echo '<th>'.$value['title']; ?>
									<i 
										id="<?php echo $value['name'].'.'.$order; ?>"
										class="material-icons sortby"
									>
										<?php echo $sortIcon; ?>										
									</i>
								</th>
							<?php 
							}
						}
					?>
				</tr>
			</thead>
			<tbody>

				<?php 
					foreach ( $fieldvalue as $key => $value) {
				?>
				<tr>
					<?php
						foreach ($fieldStructure as $fieldkey => $fieldnamevalue) {
								$class = '';

								if( $fieldnamevalue['show'] ){
									if( array_key_exists( 'details' , $fieldnamevalue ) ){

										if( $fieldnamevalue['details']['status']  )
											$class = $value [
												$fieldnamevalue['details']
												['field'] 
											];					
									}
									?>
									<td class="<?php echo $class.'_status'; ?> ">
										<?php echo $value[ $fieldnamevalue['name'] ]; ?>		
									</td>
									<?php
								}
							?>
							
					<?php
						}
					?>
				</tr>
						<?php
					}
				?>
				
			</tbody>
		</table>
	</div>
	<div id="domain-filter">
		
	</div>		
	
	<div id="pagination" class="col m12">
		
	</div>		
</div>