<?php
	include 'Config.php';

	class Database
	{  
		private $host = DATABASE_HOST;   
		private $username = DATABASE_USER;   
		private $password = DATABASE_PASSWORD;   
		private $name = DATABASE_NAME;
		public  $con = '';
		
		public function Connect() {
			if (!$this->con)
				$this->con = mysqli_connect( 
						$this->host,
						$this->username,
						$this->password,
						$this->name
				);
		}

		public function Disconnect(){
			mysqli_close($this->con);
			$this->con=false;
		}

		protected function Query( $query , $return = false ){
			$this->Connect();
			$result = mysqli_query( $this->con , $query );
			$this->Disconnect();

			if ( $return ) return $result;
		}
	}  
?>