<?php	
	$page = $_POST[ 'page' ];

	$pageCount = ceil($page['totalItem'] / $page[ 'itemLength' ]);
	$pageSize = 5;

	$pageBottom = $page[ 'currentPage' ] - $pageSize;
	$pageTop = $page[ 'currentPage' ] + $pageSize;

	if( $pageBottom < 1 ){
		$pageTop += abs( $pageBottom );
		$pageBottom = 1;
		if( $pageTop > $pageCount ) $pageTop = $pageCount;
	}
	elseif( $pageTop > $pageCount ){
		$pageBottom -= ( $pageTop - $pageCount -1 );
		$pageTop = $pageCount;

		if( $pageBottom < 1 ) $pageBottom = 1;
	}

	$pageNumberList = range( $pageBottom , $pageTop );
	$goLeft = $page['currentPage'] == 1 ? 'disabled' : 'waves-effect ';
	$goRight = ( $page['currentPage'] * $page['itemLength']  + $page['itemLength'] ) > ($page['totalItem']) ? 'disabled' : 'waves-effect ';
?>

<ul class="pagination right">
	<li class="<?php echo $goLeft; ?> pageNumber">
		<a>
			<i 
				id="1" 
				class="material-icons">
					skip_previous
			</i>
		</a>
	</li>
	<li class=" <?php echo $goLeft; ?> pageNumber">
		<a>
			<i 
				id="<?php echo $page['currentPage']-1; ?>" 
				class="material-icons">
					chevron_left
			</i>
		</a>
	</li>
	<?php
		foreach ($pageNumberList as $key => $value) {
			?>
				<li class="<?php echo $value == $page['currentPage'] ? 'active  teal lighten-1' : 'waves-effect'; ?> pageNumber"><a ><?php echo $value; ?></a></li>
			<?php
		}
	?>
	<li 
		class="<?php echo $goRight; ?> pageNumber">
			<a>
				<i 
					id="<?php echo $page['currentPage']+1; ?>" 
					class="material-icons">
					chevron_right
				</i>
			</a>
	</li>
	<li  
		class="<?php echo $goRight; ?> pageNumber">
			<a>
				<i 
					id="<?php echo $pageCount; ?>" 
					class="material-icons">
						skip_next
				</i>
			</a>
	</li>
</ul>