<?php
	
	include 'Database.php';

	class Domain extends Database {

		private $Alies = array(
			'name' => 'tbl_domainpricing.domain_name',
			'price' => 'tbl_domainpricing.domain_price' ,
			'afternicprice' => 'afternic.price',
			'afternicstatus' => 'afternic.status',
			'sedoprice' => 'sedo.price',
			'sedostatus' => 'sedo.status',
			'godaddyprice' => 'godaddy.price',
			'godaddystatus' => 'godaddy.status'

		);

		public function get( $param ){
			$keyword = $this->getKeywords( $param['keyword'] );
			$pagination = $this->getPagination( $param['pagination'] , $keyword );
			$sort = $this->getSortby( $param['sortedby'] );

			$query = 
				'SELECT 
					' .$this->Alies['name']. ' as name,
					' .$this->Alies['price']. ' as price,
					' .$this->Alies['afternicprice']. ' as afternicprice ,
					' .$this->Alies['afternicstatus']. ' as afternicstatus ,
					' .$this->Alies['sedoprice']. ' as sedoprice ,
					' .$this->Alies['sedostatus']. ' as sedostatus ,
					' .$this->Alies['godaddyprice']. ' as godaddyprice ,
					' .$this->Alies['godaddystatus']. ' as godaddystatus 
				FROM tbl_domainpricing 
				LEFT JOIN afternic on afternic.domain_id = tbl_domainpricing.id 
				LEFT JOIN sedo on sedo.domain_id = tbl_domainpricing.id 
				LEFT JOIN godaddy on godaddy.domain_id = tbl_domainpricing.id'.
				$keyword.' 
				ORDER BY '.$sort.'
				LIMIT '.$pagination['pageStart'].','.$pagination['itemLength'];

			$data = $this->toJson( 
				$this->Query( $query , true ) 
			);

			$param[ 'pagination' ] = $pagination;

			return json_encode( array( 'data' => $data , 'newfilters' => $param ) );
		}



		public function sanitize( $var , $isObj = true ){

			if( $isObj ) 
				return filter_var_array( $var , FILTER_SANITIZE_STRING);
			else 
				return filter_var($var, FILTER_SANITIZE_STRING);
		}

		private function getKeywords( $var ){
			if( $var != '' ) 
				$var = ' WHERE ' .$this->Alies['name']. ' LIKE "' . $var.'%" ';

			return $var;
		}

		private function getPagination( $obj ,$keyword ){
			$obj['totalItem'] = $this->Query( 
					'SELECT COUNT(id) as length FROM tbl_domainpricing '.$keyword , true 
			);

			$obj['totalItem'] = mysqli_fetch_assoc( $obj['totalItem'] );
			$obj['totalItem'] = $obj['totalItem']['length'];
			$obj['pageStart'] =  ( $obj['currentPage'] * $obj['itemLength'] ) - $obj['itemLength'];

			return $obj;
		}

		private function getSortby( $obj ){
			$query = $this->Alies[ $obj['field'] ].' '.$obj['value'];
			return $query;
		}

		private function toJson ( $mysqlObj ) {
			$list = [];

			while( $item = mysqli_fetch_assoc( $mysqlObj ) )
				$list[] = $item;
			
			return json_encode( $list );
		}
	}
?>