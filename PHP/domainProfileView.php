<?php
	include'connection.php';
	
	$domainID = $_GET['ID'];
	$ReadSql = "SELECT * FROM tbl_domain WHERE id = '$domainID'";
    $res = mysqli_query($conn, $ReadSql); 
?>
	<div class="modal-header">
		<h4 class="modal-title text-center"><center>Domain Profile View</center></h4>
	   	<button type="button" class="close" data-dismiss="modal">&times;</button>
	</div>
	<!-- Modal body -->
	<div class="modal-body">
    <?php       
	    if ($res->num_rows > 0) {
	      while($row = $res->fetch_assoc()) { 
	        if($row['domain_channel'] == "Snapnames") :
    ?>
	            <table class="domainView">
	              <tr>
	                <td class="tableViewLbl">Domain Name: </td>
	                <td class="tableViewData"><?php echo $row['domain_name'] ?></td>
	              </tr>
	              <tr>
	                <td class="tableViewLbl">Date: </td>
	                <td><?php echo $row['domain_date'] ?></td>
	              </tr>
	              <tr>
	                <td class="tableViewLbl">Registrar: </td>
	                <td><?php echo $row['domain_registrar'] ?></td>
	              </tr>
	              <tr>
	                <td class="tableViewLbl">ManagementUrl: </td>
	                <td><?php echo $row['domain_management'] ?></td>
	              </tr>
	              <tr>
	                <td class="tableViewLbl">AmountPaid(USD): </td>
	                <td><?php echo $row['domain_amount'] ?></td>
	              </tr>
	              <tr>
	                <td class="tableViewLbl">Account: </td>
	                <td><?php echo $row['domain_account'] ?></td>
	              </tr>
	            </table>
				<?php elseif($row['domain_channel'] == "Namejet") : ?>
					<table class="domainView">
					  <tr>
					    <td class="tableViewLbl">Domain Name: </td>
					    <td class="tableViewData"><?php echo $row['domain_name'] ?></td>
					  </tr>
					  <tr>
					    <td class="tableViewLbl">Auction Source Desc: </td>
					    <td><?php echo $row['domain_aucsource'] ?></td>
					  </tr>
					  <tr>
					    <td class="tableViewLbl">Auction Type Desc: </td>
					    <td><?php echo $row['domain_auctype'] ?></td>
					  </tr>
					  <tr>
					    <td class="tableViewLbl">Bid Amount: </td>
					    <td><?php echo $row['domain_bid'] ?></td>
					  </tr>
					  <tr>
					    <td class="tableViewLbl">Max Bid: </td>
					    <td><?php echo $row['domain_amount'] ?></td>
					  </tr>
					  <tr>
					    <td class="tableViewLbl">Auction Status Desc: </td>
					    <td><?php echo $row['domain_aucstat'] ?></td>
					  </tr>
					  <tr>
					    <td class="tableViewLbl">End Date: </td>
					    <td><?php echo $row['domain_date'] ?></td>
					  </tr>
					  <tr>
					    <td class="tableViewLbl">Payment Due Date: </td>
					    <td><?php echo $row['domain_duedate'] ?></td>
					  </tr>
					  <tr>
					    <td class="tableViewLbl">Auction ID: </td>
					    <td><?php echo $row['domain_aucid'] ?></td>
					  </tr>
					</table>
				<?php else : ?>
                    <table class="domainView">
                      <tr>
                        <td class="tableViewLbl">Receipt Number: </td>
                        <td class="tableViewData"><?php echo $row['domain_receiptnum'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">Order Date: </td>
                        <td><?php echo $row['domain_date'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">SKU: </td>
                        <td><?php echo $row['domain_sku'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">Product Name: </td>
                        <td><?php echo $row['domain_productname'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">Name: </td>
                        <td><?php echo $row['domain_name'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">unit Price: </td>
                        <td><?php echo $row['domain_amount'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">Today's Price: </td>
                        <td><?php echo $row['domain_todayprice'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">ICANN Fee: </td>
                        <td><?php echo $row['domain_icann'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">Qty: </td>
                        <td><?php echo $row['domain_qty'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">Length: </td>
                        <td><?php echo $row['domain_length'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">Extra Disc.: </td>
                        <td><?php echo $row['domain_extradisc'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">Subtotal Amount: </td>
                        <td><?php echo $row['domain_subtotal'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">Shipping and Handling Amount: </td>
                        <td><?php echo $row['domain_shipping'] ?></td>
                      </tr>
                      <tr>
                        <td class="tableViewLbl">Tax Amount: </td>
                        <td><?php echo $row['domain_tax'] ?></td>
                      </tr>
                    </table>
              <?php endif; } } ?>
	</div>

	<!-- Modal footer -->
	<div class="modal-footer">
	<button type="button" class="btn btn-danger mr-auto" data-dismiss="modal">Close</button>
	</div>