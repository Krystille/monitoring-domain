<?php
session_start();
include 'connection.php';
if($_POST) {

	$channel = $_POST['channelDomain'];
	$dateUpload = date('Y-m-d');
	$updatedBy = $_SESSION['login_id'];

	$type = explode(".",$_FILES['file']['name']);
	if(strtolower(end($type)) != 'csv'){
		echo 1;

		exit(0);
	}

	$filename=$_FILES["file"]["tmp_name"];
	

	if($_FILES["file"]["size"] > 0)
	{
		$file = fopen($filename, "r");

		$domainName = 0;
		$domainDate= 0;
		$domainAmount = 0;
		
		//Snapnames
		$domainRegistrar = 0;
		$domainManagement = 0;
		$domainAccount = 0;

		//Namejet
		$domainASD = 0;
		$domainATD = 0;
		$domainBid = 0;
		$domainAStatD = 0;
		$domainPDD = 0;
		$domainAID = 0;

		//GoDaddy
		$domainType = 0;
		$domainRecNum = 0;
		$domainSKU = 0;
		$domainTP = 0;
		$domainICANN = 0;
		$domainQty = 0;
		$domainLength = 0;
		$domainExtraD = 0;
		$domainSubtotal = 0;
		$domainShipping = 0;
		$domainTax = 0;
		$hasColumn = 0;
		$i=0;
		$isDuplicate = false;
		$isNew = false;

		while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
		{
			$i++;
			if($i==1) {
				for ($a = 0; $a < count($emapData); $a++) {
					if($emapData[$a] == "Name" || $emapData[$a] == "DomainName")
					{
						$hasColumn += 1;
						$domainName = $a;
					}
					elseif ($emapData[$a] == "EndDate" || $emapData[$a] == "Date" || $emapData[$a] == "Order date")
					{
						$hasColumn += 1; 
						$domainDate = $a;
					}
					elseif ($emapData[$a] == "Unit price" || $emapData[$a] == "MaxBid" || $emapData[$a] == "AmtPaid(USD)")
					{
						$hasColumn += 1;
						$domainAmount = $a;
					}
					elseif ($emapData[$a] == "Product name")
					{
						$hasColumn += 1;
						$domainType = $a;
					}
					elseif (trim($emapData[$a]) == "Registrar")
						$domainRegistrar = $a;
					elseif (trim($emapData[$a]) == "ManagementUrl")
						$domainManagement = $a;
					elseif (trim($emapData[$a]) == "Account")
						$domainAccount = $a;
					elseif (trim($emapData[$a]) == "AuctionSourceDesc")
						$domainASD = $a;
					elseif (trim($emapData[$a]) == "AuctionTypeDesc")
						$domainATD = $a;
					elseif (trim($emapData[$a]) == "BidAmount")
						$domainBid = $a;
					elseif (trim($emapData[$a]) == "AuctionStatusDesc")
						$domainAStatD = $a;
					elseif (trim($emapData[$a]) == "PaymentDueDate")
						$domainPDD = $a;
					elseif (trim($emapData[$a]) == "AuctionID")
						$domainAID = $a;
					elseif (rtrim($emapData[$a]) == "Receipt number")
						$domainRecNum = $a;
					elseif (trim($emapData[$a]) == "SKU")
						$domainSKU = $a;
					elseif (rtrim($emapData[$a]) == "Today's price")
						$domainTP = $a;
					elseif (rtrim($emapData[$a]) == "ICANN fee")
						$domainICANN = $a;
					elseif (trim($emapData[$a]) == "Qty")
						$domainQty = $a;
					elseif (trim($emapData[$a]) == "Length")
						$domainLength = $a;
					elseif (rtrim($emapData[$a]) == "Extra disc.")
						$domainExtraD = $a;
					elseif (rtrim($emapData[$a]) == "Subtotal amount")
						$domainSubtotal = $a;
					elseif (rtrim($emapData[$a]) == "Shipping and handling amount")
						$domainShipping = $a;
					elseif (rtrim($emapData[$a]) == "Tax amount")
						$domainTax = $a;
				}
				continue;
			}

			if($emapData[$domainType] == "Premium Listing" ||$emapData[$domainType] == "Premium Listing Renewal")
				continue;

			if($channel != "GoDaddy" && $hasColumn < 3 || $channel == "GoDaddy" && $hasColumn < 4)
			{
				echo 2;
				exit(0);
			}

			if(strtotime($emapData[$domainDate]))
				$newDate = date('Y-m-d', strtotime($emapData[$domainDate]));
			else
				$newDate = '0000-00-00';
			$num = str_replace(array(',','$'),'',$emapData[$domainAmount]);
			if(!is_numeric(trim($num)))
				$emapData[$domainAmount] = "00.00";
			else
				$emapData[$domainAmount] = number_format(trim($num), 2, '.', '');

			if($channel == "GoDaddy")
				$emapData[$domainAmount] = floatval($emapData[$domainTP]) + floatval($emapData[$domainICANN]);
			$queryDuplicate = mysqli_query($conn, "SELECT * FROM tbl_domain WHERE domain_name='$emapData[$domainName]' AND domain_date='$newDate' AND domain_amount='$emapData[$domainAmount]' AND domain_channel='$channel'");
			$rowsDuplicate = mysqli_num_rows($queryDuplicate);
			if ($rowsDuplicate < 1) {
				$isNew = true;
				if($channel == "GoDaddy")
				{
					$dType = 'New';
					if(strpos(strtolower($emapData[$domainType]), 'renewal') !== false)
						$dType = 'Renewal';
					elseif (strpos(strtolower($emapData[$domainType]), 'transfer') !== false) {
						$dType = 'Transfer';
					}

					$sql = "INSERT into tbl_domain(`domain_name`, `domain_date`, `domain_amount`, `domain_channel`, `domain_type`, `domain_dateuploaded`, `domain_updatedby`, `domain_receiptnum`, `domain_sku`, `domain_todayprice`, `domain_icann`, `domain_qty`, `domain_length`, `domain_extradisc`, `domain_subtotal`, `domain_shipping`, `domain_tax`, `domain_productname`) values('$emapData[$domainName]','$newDate','$emapData[$domainAmount]','$channel','$dType','$dateUpload','$updatedBy', '$emapData[$domainRecNum]', '$emapData[$domainSKU]', '$emapData[$domainTP]', '$emapData[$domainICANN]', '$emapData[$domainQty]', '$emapData[$domainLength]', '$emapData[$domainExtraD]', '$emapData[$domainSubtotal]', '$emapData[$domainShipping]', '$emapData[$domainTax]', '$emapData[$domainType]' )";
				}
				else if($channel == "Namejet")
				{
					$sql = "INSERT into tbl_domain(`domain_name`, `domain_date`, `domain_amount`, `domain_channel`, `domain_dateuploaded`, `domain_updatedby`, `domain_aucsource`, `domain_auctype`, `domain_bid`, `domain_aucstat`, `domain_duedate`, `domain_aucid`, `domain_type`) values('$emapData[$domainName]','$newDate','$emapData[$domainAmount]','$channel','$dateUpload','$updatedBy', '$emapData[$domainASD]', '$emapData[$domainATD]', '$emapData[$domainBid]', '$emapData[$domainAStatD]', '$emapData[$domainPDD]', '$emapData[$domainAID]', 'New')";
				}
				else
					$sql = "INSERT into tbl_domain(`domain_name`, `domain_date`, `domain_amount`, `domain_channel`, `domain_dateuploaded`, `domain_updatedby`, `domain_registrar`, `domain_management`, `domain_account`, `domain_type`) values('$emapData[$domainName]','$newDate','$emapData[$domainAmount]','$channel','$dateUpload','$updatedBy', '$emapData[$domainRegistrar]', '$emapData[$domainManagement]', '$emapData[$domainAccount]', 'New')";

				if (mysqli_query($conn, $sql)) {
				    
				} else {
				    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
				}
			}
			else
				$isDuplicate = true;

			echo " ";
   			flush();
		}
		fclose($file);

		if($isNew && $isDuplicate) {
			echo 3;
		}
		elseif (!$isNew && $isDuplicate) {
			echo 4;
		}
		else {
			echo 5;
		} 
	}
} 
?>