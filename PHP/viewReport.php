<?php
	if($_POST)
	{
		include 'connection.php';
		$channel = $_POST['channelVal'];
		$date = $_POST['dateVal'];
		$date2 = $_POST['dateVal2'];
		$view = $_POST['viewVal'];
		$type = $_POST['typeVal'];
		$startDate;
		$endDate;
		$domainlist_array = [];

		switch ($view) {
			case 'All':
				if($channel == 'All')
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id";
				elseif ($channel == 'GoDaddy' && $type != 'All') {
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_type = '$type'";
				}
				else
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel'";
				break;
			case 'Daily':
				$startDate = $date;
				$endDate = $date;
				if($channel == 'All')
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_date BETWEEN '$startDate' AND '$endDate'";
				elseif ($channel == 'GoDaddy' && $type != 'All') {
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_type = '$type' AND domain_date BETWEEN '$startDate' AND '$endDate'";
				}
				else
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_date BETWEEN '$startDate' AND '$endDate'";
				break;
			case 'Weekly':
				$startDate = $date;
				$endDate = date('Y-m-d', strtotime($date. ' + 6 days'));
				if($channel == 'All')
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_date BETWEEN '$startDate' AND '$endDate'";
				elseif ($channel == 'GoDaddy' && $type != 'All') {
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_type = '$type' AND domain_date BETWEEN '$startDate' AND '$endDate'";
				}
				else
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_date BETWEEN '$startDate' AND '$endDate'";
				break;
			case 'Monthly':
				$startDate = $date . "-01";
				$endDate = date('Y-m-t', strtotime($date . "-01"));
				if($channel == 'All')
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_date BETWEEN '$startDate' AND '$endDate'";
				elseif ($channel == 'GoDaddy' && $type != 'All') {
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_type = '$type' AND domain_date BETWEEN '$startDate' AND '$endDate'";
				}
				else
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_date BETWEEN '$startDate' AND '$endDate'";
				break;
			case 'Yearly':
				$startDate = $date . "-01-01";
				$endDate = $date . "-12-31";
				if($channel == 'All')
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_date BETWEEN '$startDate' AND '$endDate'";
				elseif ($channel == 'GoDaddy' && $type != 'All') {
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_type = '$type' AND domain_date BETWEEN '$startDate' AND '$endDate'";
				}
				else
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_date BETWEEN '$startDate' AND '$endDate'";
				break;
			case 'Periodic':
				$startDate = $date;
				$endDate = $date2;
				if($channel == 'All')
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_date BETWEEN '$startDate' AND '$endDate'";
				elseif ($channel == 'GoDaddy' && $type != 'All') {
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_type = '$type' AND domain_date BETWEEN '$startDate' AND '$endDate'";
				}
				else
					$sql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id WHERE domain_channel = '$channel' AND domain_date BETWEEN '$startDate' AND '$endDate'";
				break;
			default:
				# code...
				break;
		}
        $query = mysqli_query($conn, $sql);
        $rows = mysqli_num_rows($query);
        if ($rows >= 1) {
		    while ($data = mysqli_fetch_assoc($query)) {
				$domainlist_array[] = $data;
			}
		    echo json_encode($domainlist_array);
        }
	} 
?>