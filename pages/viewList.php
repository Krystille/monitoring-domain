<?php
include('../PHP/session.php');
include('../header.php');
?>
<!-- Page Content -->
<section>
  <div class="container contentDiv">

    <div class="row">
      <div class="col-lg-12">
        <form class="form-inline justify-content-center">
          <label>Channel:</label>&nbsp;
           <select class="form-control" id="channelSelect">
            <option value="" selected disabled>-Please select-</option>
            <option value="All">All</option>
            <option value="Snapnames">Snapnames</option>
            <option value="Namejet">Namejet</option>
            <option value="GoDaddy">GoDaddy</option>
          </select>
          <label id="domainTypeLabel">&nbsp;&nbsp;Type:</label>&nbsp;
          <select class="form-control" id="domainTypeSelect">
            <option value="" selected disabled>-Please select-</option>
            <option value="All">All</option>
            <option value="New">New</option>
            <option value="Renewal">Renewal</option>
            </select>
          <label>&nbsp;&nbsp;View By:</label>&nbsp;
          <select class="form-control" id="viewSelect" disabled="true">
            <option value="" selected disabled>-Please select-</option>
            <option value="All">All</option>
            <option value="Daily">Daily</option>
            <option value="Weekly">Weekly</option>
            <option value="Monthly">Monthly</option>
            <option value="Yearly">Yearly</option>
            <option value="Periodic">Periodic</option>
          </select>&nbsp;&nbsp;
          <label id="dateLbl"></label>
          <button type="button" class="btn btn-success" onclick="viewReport();">GENERATE</button>
        </form>
      </div>
    </div><br>

    <!-- Date Selection Modal -->
    <div class="modal fade" id="dateModal" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
        
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title text-center"><center>Choose Date To View</center></h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <form name="sentMessage" onsubmit="return selectDate();">
          <!-- Modal body -->
          <div class="modal-body">
            <div id="divDaily" style="display: none;">
              <label>Date: </label>
              <input type="text" class="form-control input-sm" id="datepickerDaily" placeholder="-Click to select date-" >
            </div>

            <div id="divWeekly" style="display: none;">
              <label>Date: </label>
              <input type="text" class="form-control input-sm" id="datepickerWeekly" placeholder="-Click to select date-" >
            </div>

            <div id="divMonthly" style="display: none;">
              <label>Date: </label>
              <input type="text" class="form-control input-sm" id="datepickerMonthly" placeholder="-Click to select date-" >
            </div>

            <div id="divYearly" style="display: none;">
              <label>Date: </label>
              <input type="text" class="form-control input-sm" id="datepickerYearly" placeholder="-Click to select date-" >
            </div>
            <div id="divPeriodic" style="display: none;">
              <label>Start Date: </label>
              <input type="text" class="form-control input-sm" id="datepickerPeriodic1" placeholder="-Click to select date-" >
              <label>End Date: </label>
              <input type="text" class="form-control input-sm" id="datepickerPeriodic2" placeholder="-Click to select date-" >
            </div>
          </div>
          
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="submit" class="btn btn-success mr-auto">SELECT</button>
          </div>
          </form>
        </div>
      </div>
    </div>

    <!-- Domain Profile Modal -->
    <div class="modal fade openModal" id="domainViewModal" data-keyboard="false" data-backdrop="static">
      <div class="modal-dialog modal-dialog-centered modalProfile">
        <div class="modal-content domainContent">
          
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-lg-12">

        <?php
          include('../PHP/connection.php');
          $ReadSql = "SELECT tbl_domain.*, username FROM tbl_domain INNER JOIN tbl_users ON tbl_domain.domain_updatedby = tbl_users.id";
          $res = mysqli_query($conn, $ReadSql); 

          if ($res->num_rows > 0) {
            echo "<script>plsWaitDiv('body', 'show');</script>";
        ?>
        <table class="table table-bordered nowrap display" id="tbldomain">
          <thead>
            <tr>
              <th>Domain Name</th>
              <th>Date</th>
              <th>Amount(USD)</th>
              <th>Channel</th>
              <th>Type</th>
              <th>Date Uploaded</th>
              <th>Updated By</th>
              <th>View</th>
            </tr>
          </thead>
          <tbody id="tbodyid">
            <?php 
              while($row = $res->fetch_assoc()) {
                $num = floatval(str_replace('$', '', $row["domain_amount"]));
                  echo "<tr><td>" . $row["domain_name"]. "</td><td>" . $row["domain_date"]. "</td><td>" . number_format($num, 2). "</td><td>" . $row["domain_channel"]. "</td><td>" . $row["domain_type"]. "</td><td>" . $row["domain_dateuploaded"]. "</td><td>" . $row["username"]. "</td><td><a href='' class='openModal' data-toggle='modal' data-target='#domainViewModal' data-id='".$row['id']."'><span class='fa fa-search'></span> View</a></td></tr>";
              }
              echo "<script>plsWaitDiv('body', 'hide');</script>";
            ?>
          </tbody>
          <tfoot>
            <tr>
                <th colspan="8" class="text-right">Total:</th>
            </tr>
        </tfoot>
        </table>
        <div class="btnExport"></div>
        <?php 
          }
          else
            echo "<br><center><h4>No List Yet</h4></center>"; 
        ?>
      </div>
    </div>
  </div>
</section>

<?php
//include('../footer.php');
?>