<?php
include('../PHP/session.php');
include('../header.php');
?>
<!-- Page Content -->
<section>
  <div class="contentDiv">
    <!-- <div class="row">
      <div class="col-lg-12">
        <form class="form-inline justify-content-center">
          <label>Channel:</label>&nbsp;
           <select class="form-control" id="channelPricingSelect">
            <option value="" selected disabled>-Please select-</option>
            <option value="All">All</option>
            <option value="Afternic">Afternic</option>
            <option value="Sedo">Sedo</option>
            <option value="GoDaddy">GoDaddy</option>
          </select>&nbsp;&nbsp;
          <button type="button" class="btn btn-success" onclick="viewDomainPricing();">GENERATE</button>
        </form>
      </div>
    </div><br> -->

    <div class="row">
      <div class="col-lg-12">

        <?php
          include('../PHP/connection.php');
          $ReadSql = "SELECT main.domain_name, a.price AS afternicPrice, s.price as sedoPrice, g.price AS godaddyPrice, a.status AS afternicStatus, s.status AS sedoStatus, g.status AS godaddyStatus, a.updated_at AS afternicUpdated, s.updated_at AS sedoUpdated, g.updated_at AS godaddyUpdated, pa.ip AS afternicIP, ps.ip AS sedoIP, pg.ip AS godaddyIP FROM tbl_domainpricing main INNER JOIN afternic a ON main.id = a.domain_id INNER JOIN sedo s ON main.id = s.domain_id INNER JOIN godaddy g ON main.id = g.domain_id INNER JOIN proxies pa ON a.proxy_id = pa.id INNER JOIN proxies ps ON s.proxy_id = ps.id INNER JOIN proxies pg ON g.proxy_id = pg.id";
          $res = mysqli_query($conn, $ReadSql); 

          if ($res->num_rows > 0) {
            echo "<script>plsWaitDiv('body', 'show');</script>";
        ?>
        <table class="table table-bordered nowrap display" id="tbldomainCompare">
          <thead>
            <tr>
              <th>Domain Name</th>
              <th>Afternic Price(USD)</th>
              <th>Afternic Status</th>
              <th>Afternic Updated</th>
              <th>Afternic IP</th>
              <th>Sedo Price(USD)</th>
              <th>Sedo Status</th>
              <th>Sedo Updated</th>
              <th>Sedo IP</th>
              <th>GoDaddy Price(USD)</th>
              <th>GoDaddy Status</th>
              <th>GoDaddy Updated</th>
              <th>GoDaddy IP</th>
              
            </tr>
          </thead>
          <tbody id="tbodyidprice">
            <?php 
              while($row = $res->fetch_assoc()) {
                $num = floatval(str_replace('$', '', $row["domain_price"]));
                echo "<tr><td>" . $row["domain_name"]. "</td><td>" . $row["afternicPrice"]. "</td><td>" . $row["afternicStatus"]. "</td><td>" . $row["afternicUpdated"]. "</td><td>" . $row["afternicIP"] . "</td><td>" . $row["sedoPrice"]. "</td><td>" . $row["sedoStatus"]. "</td><td>" . $row["sedoUpdated"]. "</td><td>" . $row["sedoIP"] . "</td><td>" . $row["godaddyPrice"]. "</td><td>" . $row["godaddyStatus"]. "</td><td>" . $row["godaddyUpdated"]. "</td><td>" . $row["godaddyIP"] . "</td></tr>";
              }
              echo "<script>plsWaitDiv('body', 'hide');</script>";
            ?>
          </tbody>
        </table>
        <?php 
          }
          else
            echo "<br><center><h4>No List Yet</h4></center>"; 
        ?>
      </div>
    </div>
  </div>
</section>