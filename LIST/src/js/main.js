$( document ).ready(function() {
	/*
	*================ FUNCTIONS ====================
	*/
	renderDomainList = () => {
		loading()

		$.post( realpath , { request: 'get' , filters : JSON.stringify( filters )   } )
			.done( ( result )=>{			
				const { data , newfilters } = JSON.parse( result )

				$.post( views.list ,				
					{
						fieldname: JSON.stringify( newfilters.field ),
						fieldvalue: data,
						page: newfilters,
						sortby: newfilters.sortedby
					}
				).done( (view) => {
					$('main').html( view);
					renderFilter()
					renderPagination()
					loading( true )
				})

				filters.pagination = newfilters.pagination;
		})
	}

	renderPagination = () => {			
		$.post( views.pagination ,
			{
				page: filters.pagination 
			}
		).done( (view) => {			
			$('#pagination').html( view );
		})
	}

	renderFilter = () => {
		$.post( views.domainfilter , 
			{
				filters: JSON.stringify( filters ) 
			} 
		).done( (view) => {
			$('#domain-filter').html( view )
			$('.sidenav').sidenav()
		})
	}

	loading = ( $active = false ) => {
		if( $active )
			 $('#loading').css( 'display','none');
		else  $('#loading').css( 'display', 'block' );
	}

	/*
	*================ INITIALIZATION ====================
	*/
	const realpath = 'src/class/AjaxRequest.php';
	const views = {
		list: 'src/views/domainlist.php',
		pagination: 'src/views/pagination.php',
		domainfilter: 'src/views/domainfilter.php'
	}

	let filters = {
		field: [
			{
				name: 'name',
				title:'name',
				show: true
			},
			{
				name: 'price',
				title:'price',
				show: true
			},
			{
				name: 'afternicprice',
				title:'afternic',
				details: {
					price:true,
					field:'afternicstatus',
					status:true
				},
				show: true
			},
			{
				name: 'sedoprice',
				title:'sedo',
				details: {
					price:true,
					field:'sedostatus',
					status:true
				},
				show: true
			},
			{
				name: 'godaddyprice',
				title:'godaddy',
				details: {
					price:true,
					field:'godaddystatus',
					status:true
				},
				show: true
			}
		],
		keyword: '',
		sortedby: { 
			field: 'name',
			value: 'asc' 
		},
		pagination: {
			totalItem: false,
			currentPage: 1,
			itemLength: 10
		}
	}

	renderDomainList()

	/*
	*================ EVENT HANDLE ====================
	*/

	//change page
	$('main').on( 'click' , '.pageNumber', ( e ) => {
		let pageNumber =   $( e.target ).text().trim();		
		skipPage = [ 
			'skip_previous' ,
			'chevron_left',
			'chevron_right',
			'skip_next'
		];

		if ( skipPage.includes( pageNumber ) )
			pageNumber = $( e.target ).attr('id')
		
		filters.pagination.currentPage =  pageNumber
	
		renderDomainList()
		e.stopImmediatePropagation()
		e.preventDefault()
		
	})

	//apply filter
	$('main').on( 'click', '#apply-filter' , (e) => {
		$('.sidenav').sidenav('close')
		//update checkbox filter
		let checkboxs = [];

		$('.filter_fields_checkbox').each( (key,element) => {
			 let temp = element.id.split('.');
			 temp.push( element.checked )
			 checkboxs.push( temp )
		} )

		filters.field.forEach( (value,key) => {
			checkboxs.forEach( (checkValue,checkKey) => {
				if( value.name == checkValue[0] ){
					//special case
					if( checkValue[2] == 'price' ){
						filters.field[key]['show'] = checkValue[3]
					}
					filters.field[key].details[checkValue[2]] = checkValue[3]
					
				}
			})
		})

		filters.keyword = $('#filter_keywords').val().trim()
		filters.pagination.currentPage =  1;
		renderDomainList()
	})

	// item per page control
	$('main').on( 'mouseup' , '#itemLength', e => {
		$('.page-range span')[0].innerText= 'page: ' + $('#itemLength').val()
		filters.pagination.itemLength = parseInt( $('#itemLength').val() )
	} )

	//sort by
	$('main').on( 'click' , '.sortby', (e) =>{
		let order = $(e.target).attr('id').trim().split('.');

		filters.sortedby.field = order[0]
		filters.sortedby.value = order[1] == 'asc' ? 'desc' : 'asc';
		renderDomainList()

	})
});
