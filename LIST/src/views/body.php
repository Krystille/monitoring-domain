<header class="teal darken-2 z-depth-3 hoverable">
	
	<nav id="main-nav" class="container teal darken-2 ">
		<div class="nav-wrapper">

			<a href="" class="brand-logo">Domain pricing</a>
			<ul class="right">
		        <li><a href="">Domains</a></li>
		        <li><a href="">Whois</a></li>
	     	</ul>
		</div>
	</nav>
	<div id="filter-body">
		
	</div>
</header>

<main class="container">
</main>
<footer>
	<div id="loading" >
		<div class="preloader-wrapper active">
		    <div class="spinner-layer">
		      <div class="circle-clipper left">
		        <div class="circle"></div>
		      </div><div class="gap-patch">
		        <div class="circle"></div>
		      </div><div class="circle-clipper right">
		        <div class="circle"></div>
		      </div>
		    </div>
	  </div>
	</div>
</footer>