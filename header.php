<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>D O M A I N</title>
<link rel="shortcut icon" href="http://monitoring.domainva.com/img/favicon.ico">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../css/style.css">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<script src="../js/jquery-1.12.4.js"></script>

<script type="text/javascript" src="../js/index.js"></script>

<script src="../js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="../js/dataTables.bootstrap4.min.js"></script>

<script src="../js/popper.min.js"></script>
<script src="../js/bootstrap.min.js"></script>

<link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="../css/dataTables.bootstrap4.min.css">

<script type="text/javascript" src="../js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="../css/datepicker.min.css">

<script type="text/javascript" src="../js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="../js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script> -->
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/plug-ins/1.10.16/dataRender/ellipsis.js"></script>
    
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="navbar-header">
        <img class="logo pull-left" src="http://monitoring.domainva.com/img/upload2.png"><a class="navbar-brand" href="#">&nbsp;&nbsp; Uploader Tool</a>
        </div>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <?php 
            if(isset($_SESSION['login_user'])):  ?>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        Domain List
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="../pages/home.php" class="dropdown-item">
                            Upload Report
                        </a>
                        <a href="../pages/viewList.php" class="dropdown-item">
                            View List
                        </a>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        Domain Pricing
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a href="../pages/uploadPricing.php" class="dropdown-item">
                            Upload Domain Pricing
                        </a>   
                        <a href="../pages/compareList.php" class="dropdown-item">
                            View Domain Pricing
                        </a>
                        <a href="../pages/debugList.php" class="dropdown-item">
                            For debug
                        </a>     
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        Welcome <?php echo trim($_SESSION['login_user']); ?>! <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <?php if($_SESSION['login_role'] == 1): ?>
                            <a class="dropdown-item" href="../pages/addUserPage.php">Add User</a>
                        <?php endif; ?>
                        <a class="dropdown-item" href="../PHP/logout.php">Log-out</a>
                    </div>
                </li>
            <?php else: ?>
                
            <?php endif; ?>    
          </ul>
        </div>
    </nav>
<div id="pageContent">