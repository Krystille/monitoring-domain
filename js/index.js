$(function () {
    $('#domainTypeLabel').hide();
    $('#domainTypeSelect').hide();

    /*$( "#uploadForm" ).submit(function(event) {
      plsWaitDiv('body', 'show');
    });*/

    $('.custom-file-input').on('change',function(){
      $(this).next('.custom-file-label').addClass("selected").html($(this).val());
    });

    var table = $('table.display').DataTable({
      "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
      "order": [[ 1, "desc" ]],
      "createdRow": function( row, data, dataIndex){
                if( data[4] ==  `Renewal`){
                    $(row).addClass('red');
                }
      },
      "deferRender": true,
      "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 2 ,{'search': 'applied'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 2, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 2 ).footer() ).html(
                '$'+pageTotal.toLocaleString("en-US", {style: "decimal", minimumFractionDigits: 2}) +' ( $'+ total.toLocaleString("en-US", {style: "decimal", minimumFractionDigits: 2}) +' total)'
            );
        },
        columnDefs: [ {
            targets: 0,
            render: $.fn.dataTable.render.ellipsis( 25 )
        }]
    });

    $.fn.dataTable.render.ellipsis = function ( cutoff ) {
    return function ( data, type, row ) {
        if ( type === 'display' ) {
            var str = data.toString(); // cast numbers
 
            return str.length < cutoff ?
                str :
                str.substr(0, cutoff-1) +'&#8230;';
        }
 
        // Search, order and type can use the original data
        return data;
      };
    };

    if($('.btnExport').length)
    {
      var buttons = new $.fn.dataTable.Buttons(table, {
        buttons: [
          {"extend": 'csvHtml5', "text":'<span class="glyphicon glyphicon-download"></span> Download Report',"className": 'btn btn-danger', "title": 'Monitoring Domain', "exportOptions": "{orthogonal: 'export' }"}
        ]
      }).container().appendTo($('.btnExport'));
    }

    $("#datepickerDaily").datepicker( {
      format: "yyyy-mm-dd",
      autoclose: true
    });

    $("#datepickerWeekly").datepicker( {
      format: "yyyy-mm-dd",
      autoclose: true
    });

    $("#datepickerMonthly").datepicker( {
      format: "yyyy-mm",
      viewMode: "months", 
      minViewMode: "months",
      autoclose: true
    });

    $("#datepickerYearly").datepicker( {
      format: "yyyy",
      weekStart: 1,
      orientation: "bottom",
      language: "{{ app.request.locale }}",
      keyboardNavigation: false,
      viewMode: "years",
      minViewMode: "years",
      autoclose: true
    });

    $("#datepickerPeriodic1").datepicker( {
      format: "yyyy-mm-dd",
      autoclose: true
    });

    $("#datepickerPeriodic2").datepicker( {
      format: "yyyy-mm-dd",
      autoclose: true
    });

    $('#viewSelect').mouseup(function(){

      var open = $(this).data("isopen");
    
      if(open) {
        if($("#viewSelect").val() == "All")
        {
          $("#dateLbl").text('');
        }
        else if($("#viewSelect").val() == "Daily")
        {
          $("#datepickerDaily").attr('required','required');
          $("#datepickerDaily").val('');
          $('#dateModal').modal('show');
          $('#divDaily').show();
          $('#divWeekly').hide();
          $('#divMonthly').hide();
          $('#divYearly').hide();
          $('#divPeriodic').hide();
        }
        else if($("#viewSelect").val() == "Weekly")
        {
          $("#datepickerWeekly").attr('required','required');
          $("#datepickerWeekly").val('');
          $('#dateModal').modal('show'); 
          $('#divWeekly').show(); 
          $('#divDaily').hide();
          $('#divMonthly').hide();
          $('#divYearly').hide();
          $('#divPeriodic').hide();
        }
        else if($("#viewSelect").val() == "Monthly")
        {
          $("#datepickerMonthly").attr('required','required');
          $("#datepickerMonthly").val('');
          $('#dateModal').modal('show');  
          $('#divMonthly').show();
          $('#divWeekly').hide();
          $('#divDaily').hide();
          $('#divYearly').hide();
          $('#divPeriodic').hide();
        }
        else if($("#viewSelect").val() == "Yearly")
        {
          $("#datepickerYearly").attr('required','required');
          $("#datepickerYearly").val('');
          $('#dateModal').modal('show');
          $('#divYearly').show();
          $('#divWeekly').hide();
          $('#divMonthly').hide();
          $('#divDaily').hide();
          $('#divPeriodic').hide();
        }
        else if($("#viewSelect").val() == "Periodic")
        {
          $("#datepickerPeriodic1").attr('required','required');
          $("#datepickerPeriodic2").attr('required','required');
          $("#datepickerPeriodic1").val('');
          $("#datepickerPeriodic2").val('');
          $('#dateModal').modal('show');
          $('#divPeriodic').show();
          $('#divYearly').hide();
          $('#divWeekly').hide();
          $('#divMonthly').hide();
          $('#divDaily').hide();
        }
      }
      
      $(this).data("isopen", !open);
    });

    $('#channelSelect').change(function(){
      $('#viewSelect').removeAttr('disabled');
      if($('#channelSelect').val() == "GoDaddy") {
        $('#domainTypeSelect').prop("selectedIndex", 0);
        $('#domainTypeLabel').show();
        $('#domainTypeSelect').show();
      }
      else {
        $('#domainTypeLabel').hide();
        $('#domainTypeSelect').hide();
      }
    });

	$('.openModal').on('show.bs.modal', function(e) {
		var id = $(e.relatedTarget).data('id')
		console.log("ID: " + id);
		$.ajax({url:"../PHP/domainProfileView.php?ID="+id,cache:false,success:function(result){
		  $(".domainContent").html(result);
		}});
  });

  $("#dateModal").on("hidden.bs.modal", function () {
    $('#datepickerDaily').removeAttr('required');
    $('#datepickerWeekly').removeAttr('required');
    $('#datepickerMonthly').removeAttr('required');
    $('#datepickerYearly').removeAttr('required');
    $('#datepickerPeriodic1').removeAttr('required');
    $('#datepickerPeriodic2').removeAttr('required');
  });
});

function viewReport()
{
	var channel = $('#channelSelect').val();
	var viewBy = $('#viewSelect').val();
	var domainType = $('#domainTypeSelect').val();
	var dateSelected;
  var dateSelected2 = "00-00-0000";
	if(channel == null)
	{
		alert("Please select Channel.");
		$('#dateModal').modal('hide');
		return false;
	}
	else if(viewBy != null)
	{

		switch(viewBy)
		{
			case "All":
				dateSelected = "00-00-0000";
				break;
			case "Daily":
				dateSelected = $('#datepickerDaily').val();
				break;
			case "Weekly":
				dateSelected = $('#datepickerWeekly').val();
				break;
			case "Monthly":
				dateSelected = $('#datepickerMonthly').val();
				break;
			case "Yearly":
				dateSelected = $('#datepickerYearly').val();
				break;
      case "Periodic":
        dateSelected = $('#datepickerPeriodic1').val();
        dateSelected2 = $('#datepickerPeriodic2').val();
        break;
		}
	}

	if(dateSelected == "" || dateSelected == undefined)
	{
		alert("Please select date.");
		return false;
	}

	if(domainType == "" || domainType == undefined)
		domainType = "All";

	$("#tbodyid").empty();
  plsWaitDiv('body', 'show');
	$.ajax({
        type: "POST",
        url: "../PHP/viewReport.php",
        data: {
          channelVal: channel,
          viewVal: viewBy,
          dateVal: dateSelected,
          dateVal2: dateSelected2,
          typeVal: domainType
        },
        cache: false,
        dataType: "json",
        success: function(result){
            console.log(result);
          	var table = $('#tbldomain').DataTable();
          	table.search('').clear().draw();
            var addData = [];
 
          	for(var data in result){
				      addData.push([result[data].domain_name, result[data].domain_date, parseFloat(result[data].domain_amount.replace("$", "")).toFixed(2), result[data].domain_channel, result[data].domain_type, result[data].domain_dateuploaded, result[data].username, "<a href='' class='openModal' data-toggle='modal' data-target='#domainViewModal' data-id='"+result[data].id+"'><span class='fa fa-search'></span> View</a>"]);
              /*addData.push(["<a href='' class='openModal' data-toggle='modal' data-target='#domainViewModal' data-id='"+result[data].id+"'>" + result[data].domain_name + "</a>", result[data].domain_date, parseFloat(result[data].domain_amount.replace("$", "")).toFixed(2), result[data].domain_channel, result[data].domain_type, result[data].domain_dateuploaded, result[data].username]);*/
            }
            $("#tbldomain").dataTable().fnAddData(addData);
            plsWaitDiv('body', 'hide');
        },
        error: function (response) {
        	console.log("error: ", response);
        	$('#tbldomain').DataTable().clear().draw();
          plsWaitDiv('body', 'hide');
        }
    });
}

function check() {
	if (document.getElementById('confirmPasswordNew').value != ""){
	    if (document.getElementById('passwordNew').value == document.getElementById('confirmPasswordNew').value) {
	    	document.getElementById('confirmPasswordNew').style.borderColor = 'green';
	    	$('#message').hide();
	    }
		else {
			document.getElementById('confirmPasswordNew').style.borderColor = 'red';
		    document.getElementById('message').style.color = 'red';
		    document.getElementById('message').innerHTML = 'Confirmation Password did not match with New Password!';
		}
	}
	else{
		document.getElementById('confirmPasswordNew').style.borderColor = '#ced4da';
	    document.getElementById('message').innerHTML = '';
	}
}

function forgotPassword() {
	var password = Math.random().toString(36).substring(7);
}

function selectDate() {
	var viewBy = $('#viewSelect').val();
	var dateSelected;
	switch(viewBy)
	{
		case "Daily":
			dateSelected = $('#datepickerDaily').val();
			$("#dateLbl").text("Date: " + dateSelected);
			break;
		case "Weekly":
			dateSelected = $('#datepickerWeekly').val();
			var theDate = new Date(dateSelected);
			theDate.setDate(theDate.getDate()+6);
			var endDate = theDate.getUTCFullYear() + '-' + ("0" + (theDate.getMonth() + 1)).slice(-2) + '-' + ("0" + theDate.getDate()).slice(-2);
			$("#dateLbl").text("Date: " + dateSelected + " to " + endDate);
			break;
		case "Monthly":
			dateSelected = $('#datepickerMonthly').val();
			$("#dateLbl").text("Date: " + dateSelected);
			break;
		case "Yearly":
			dateSelected = $('#datepickerYearly').val();
			$("#dateLbl").text("Date: " + dateSelected);
			break;
    case "Periodic":
      dateSelected = $('#datepickerPeriodic1').val();
      var endDate = $('#datepickerPeriodic2').val();
      if(dateSelected > endDate)
        return false;
      $("#dateLbl").text("Date: " + dateSelected + " to " + endDate);
      break;
	}

	$('#dateModal').modal('hide');
  return false;
}

function plsWaitDiv(selector, visibility) {
    if ('show' == visibility) {
        $(selector).css('position', 'relative');
        $(selector).append('<div  class="plswait_overlay">Please wait<span class="dots"><span>.</span><span>.</span><span>.</span></span></div>');
    } else {
        $(selector).css('position', 'static');
        $('.plswait_overlay').remove();
    }
}

function uploadFile()
{
  plsWaitDiv('body', 'show');
  var formData = new FormData(document.getElementById("uploadForm"));
  $.ajax({
       type: 'POST',
       url: '../PHP/import.php',
       data: formData,
       cache: false,
       contentType: false,
       enctype: 'multipart/form-data',
       processData: false,
       success: function (response) {
        console.log(response);
        plsWaitDiv('body', 'hide');
        if(response == 1) {
          alert("Invalid File:Please Upload CSV File.");
          window.location = "../pages/home.php";
        }
        else if(response == 2) {
          alert("Incorrect format! Column names used do not match.");
          window.location = "../pages/home.php";
        }
        else if(response == 3) {
          alert("Some rows already exist in the list. Unique rows have been successfully imported.");
          window.location = "../pages/viewList.php";
        }
        else if(response == 4) {
          alert("Unable to insert record. Duplicate File Uploaded.");
          window.location = "../pages/home.php";
        }
        else if(response == 5) {
          alert("CSV File has been successfully Imported.");
          window.location = "../pages/viewList.php";
        }
        else
          console.log("Error: " + response);

       },
       error: function (result) {
        plsWaitDiv('body', 'hide');
        console.log(result);
       }
   });
   return false;
}

function uploadFilePricing()
{
  plsWaitDiv('body', 'show');
  var formData = new FormData(document.getElementById("uploadFormPricing"));
  $.ajax({
       type: 'POST',
       url: '../PHP/importDomainPricing.php',
       data: formData,
       cache: false,
       contentType: false,
       enctype: 'multipart/form-data',
       processData: false,
       success: function (response) {
        console.log(response);
        plsWaitDiv('body', 'hide');
        if(response == 1) {
          alert("Invalid File:Please Upload CSV File.");
          window.location = "../pages/home.php";
        }
        else if(response == 2) {
          alert("Incorrect format! Column names used do not match.");
          window.location = "../pages/uploadPricing.php";
        }
        else if(response == 3) {
          alert("Some rows already exist in the list. Unique rows have been successfully imported.");
          window.location = "../pages/compareList.php";
        }
        else if(response == 4) {
          alert("Unable to insert record. Duplicate File Uploaded.");
          window.location = "../pages/uploadPricing.php";
        }
        else if(response == 5) {
          alert("CSV File has been successfully Imported.");
          window.location = "../pages/compareList.php";
        }
        else
          console.log("Error: " + response);

       },
       error: function (result) {
        plsWaitDiv('body', 'hide');
        console.log(result);
       }
   });
   return false;
}

function deleteDomain(id) {
  var r = confirm("Are you sure you want to delete this record?");
  if (r == true) {
      $.ajax({
        type: "POST",
        url: "../PHP/deleteDomainPricing.php",
        data: {
          domainID: id
        },
        cache: false,
        success: function(result){
            console.log(result);
            alert(result);
            location.reload();
        },
        error: function (response) {
          console.log("Error: ", response);
        }
    });
  }
}

function viewDomainPricing()
{
  var channel = $('#channelPricingSelect').val();
  if(channel == null)
  {
    alert("Please select Channel.");
    return false;
  }
  
  $("#tbodyidprice").empty();
  plsWaitDiv('body', 'show');
  $.ajax({
        type: "POST",
        url: "../PHP/viewDomainPricing.php",
        data: {
          channelVal: channel,
        },
        cache: false,
        dataType: "json",
        success: function(result){
            console.log(result);
            var table = $('#tbldomainCompare').DataTable();
            table.search('').clear().draw();
            var addData = [];
            var comparedPrice = 0;
            for(var data in result){
              if(result[data].domain_channel == 'Afternic')
                  comparedPrice = result[data].afternic_price;
                else if(result[data].domain_channel == 'Sedo')
                  comparedPrice = result[data].sedo_price;
                else
                  comparedPrice = result[data].godaddy_price;

              addData.push([result[data].domain_name, parseFloat(result[data].domain_price.replace("$", "")).toFixed(2), comparedPrice, result[data].domain_channel, "<button class='btn btn-danger btn-sm' onclick='deleteDomain("+ result[data].id +")'><span class='fa fa-trash'></span></button>"]);
            }
            $("#tbldomainCompare").dataTable().fnAddData(addData);
            plsWaitDiv('body', 'hide');
        },
        error: function (response) {
          console.log("error: ", response);
          $('#tbldomainCompare').DataTable().clear().draw();
          plsWaitDiv('body', 'hide');
        }
    });
}