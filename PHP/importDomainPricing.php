<?php
session_start();
include 'connection.php';
if($_POST) {
	
	$type = explode(".",$_FILES['file']['name']);
	if(strtolower(end($type)) != 'csv'){
		echo 1;

		exit(0);
	}

	$filename=$_FILES["file"]["tmp_name"];
	

	if($_FILES["file"]["size"] > 0)
	{
		$file = fopen($filename, "r");

		$domainName = 0;
		$domainPrice = 0;
		$hasColumn = 0;
		$i=0;
		$isDuplicate = false;
		$isNew = false;

		while (($emapData = fgetcsv($file, 10000, ",")) !== FALSE)
		{
			$i++;
			if($i==1) {
				for ($a = 0; $a < count($emapData); $a++) {
					if($emapData[$a] == "Domain Name" || $emapData[$a] == "Name")
					{
						$hasColumn += 1;
						$domainName = $a;
					}
					elseif ($emapData[$a] == "Price")
					{
						$hasColumn += 1; 
						$domainPrice = $a;
					}
				}
				continue;
			}

			if($hasColumn < 2)
			{
				echo 2;
				exit(0);
			}

			$num = str_replace(array(',','$'),'',$emapData[$domainPrice]);
			if(!is_numeric(trim($num)))
				$emapData[$domainPrice] = "00.00";
			else
				$emapData[$domainPrice] = number_format(trim($num), 2, '.', '');

			$queryDuplicate = mysqli_query($conn, "SELECT * FROM tbl_domainpricing WHERE domain_name='$emapData[$domainName]' AND domain_price='$emapData[$domainPrice]'");
			$rowsDuplicate = mysqli_num_rows($queryDuplicate);
			if ($rowsDuplicate < 1) {
				$isNew = true;
				$sql = "INSERT into tbl_domainpricing(`domain_name`, `domain_price`) values('$emapData[$domainName]','$emapData[$domainPrice]')";

				if (mysqli_query($conn, $sql)) {
				    
				} else {
				    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
				}
			}
			else
				$isDuplicate = true;

			echo " ";
   			flush();
		}
		fclose($file);

		if($isNew && $isDuplicate) {
			echo 3;
		}
		elseif (!$isNew && $isDuplicate) {
			echo 4;
		}
		else {
			echo 5;
		} 
	}
} 
?>